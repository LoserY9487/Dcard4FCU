利用Dcard新API進行逢甲版文章的蒐集，

總計蒐集8000餘筆資料。

(全部抓下來大概耗費12個多小時，不過我沒做平行化的寫法，所以比較慢。)

csv與xlsx都是資料檔案，

DcardFCU.R則是我運作的語法檔，

如果要載Dcard資料記得要載入 `jsonlite` 套件，才能進行解析。

另外DcardFCUrd.RData是R獨有的資料檔案，

可以利用load('~/DcardFCUrd.RData')的方式讀檔！